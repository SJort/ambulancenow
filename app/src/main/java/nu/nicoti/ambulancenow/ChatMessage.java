package nu.nicoti.ambulancenow;

public class ChatMessage {

    private String message = "";
    private boolean isReceived;
    private int messageId;
    private String timestamp;
    private String pictureLocation;
    private int userId;
    private int ticketId = 1;

    public ChatMessage(String message, boolean isReceived, int messageId, String timestamp, String pictureLocation, int userId, int tickedId) {
        this.message = message;
        this.isReceived = isReceived;
        this.messageId = messageId;
        this.timestamp = timestamp;
        this.pictureLocation = pictureLocation;
        this.userId = userId;
        this.ticketId = tickedId;
    }


    public int getMessageId() {
        return messageId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getPictureLocation() {
        return pictureLocation;
    }

    public int getUserId() {
        return userId;
    }


    public ChatMessage(String message, boolean isReceived) {
        this.message = message;
        this.isReceived = isReceived;
    }

    public String getMessage() {
        return message;
    }

    public boolean isReceived() {
        return isReceived;
    }

}
