package nu.nicoti.ambulancenow;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

//the button listener is in the activity
public class ChatFragment extends Fragment implements ChatListAdapter.OnItemClickListener{

    private RecyclerView mRecyclerView;
    //basically the bridge between data and the view, makes the view not load the whole arraylist but only what it needs
    private ChatListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //makes the custom menu bar possible
        setHasOptionsMenu(true);

        mRecyclerView = view.findViewById(R.id.hospitalRecyclerView);
        mLayoutManager = new LinearLayoutManager(view.getContext());
        mAdapter = new ChatListAdapter(Statics.connection.getChat());

        mAdapter.setOnItemClickedListener(this);

        mRecyclerView.setHasFixedSize(true); //increases performance, when you know the view wont change in size
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

        setupInput();
    }

    public void setupInput(){
        TextInputEditText textInputEditText = getActivity().findViewById(R.id.sendMessageInputField);
        textInputEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
            }
        });
    }

    //this activates our custom menu with search
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        MenuInflater inflater = menuInflater;
        inflater.inflate(R.menu.hospital_search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        //now its basically a text field or something
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    public RecyclerView getRecyclerView(){
        return mRecyclerView;
    }


    @Override
    public void onItemClicked(int position) {

        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("HospitalChatMessage", Statics.connection.getChat().get(position).getMessage()); //do something with it
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getContext(), "Copied '" + Statics.connection.getChat().get(position).getMessage() + "' to clipboard", Toast.LENGTH_SHORT).show();
    }

    boolean shouldRun = false;

    @Override
    public void onResume() {
        Statics.log("ChatFragment onResume()");
        new leThread().execute("kanker");
        shouldRun = true;
        super.onResume();
    }

    @Override
    public void onPause() {
        Statics.log("ChatFragment onPause()");
        shouldRun = false;
        super.onPause();
    }


    public boolean areThereNewChatMessages(String jason){
        try{
            ArrayList<ChatMessage> tempChatMessages= new ArrayList<>();

            JsonElement jsonElement = new JsonParser().parse(jason);
            if(jsonElement == null){
                Statics.log("jsonElement was null");
                return false;
            }
            if(jsonElement.isJsonArray()){
                JsonArray jsonArray = jsonElement.getAsJsonArray();
                for(JsonElement element : jsonArray){
                    if(element.isJsonObject()){
                        if(element == null){
                            Statics.log("Element was null");
                            continue;
                        }
                        //{"Message_id":11,"timestamp":"2019-01-31 12:33:32","Picture_location":"\/..\/foler\/pic11.png","User_id":5,"Ticket_id":3,"Message_String":"ahhhh"}
                        String messageId = element.getAsJsonObject().get("Message_id").getAsString();
                        String timestamp = element.getAsJsonObject().get("timestamp").getAsString();
                        String pictureLocation = element.getAsJsonObject().get("Picture_location").getAsString();
                        String userId = element.getAsJsonObject().get("User_id").getAsString();
                        String ticketId = element.getAsJsonObject().get("Ticket_id").getAsString();
                        String message = element.getAsJsonObject().get("Message_String").getAsString();

                        int userIdInt = Integer.parseInt(userId);
                        int messageIdInt = Integer.parseInt(messageId);
                        int ticketIdInt = Integer.parseInt(ticketId);
                        boolean isFromUs = userIdInt == Statics.userId;
                        tempChatMessages.add(new ChatMessage(message, !isFromUs, messageIdInt, timestamp, pictureLocation, userIdInt, ticketIdInt));
                    }
                    else{
                        Statics.log("Element in array is not a json object.");
                    }

                }

                if(Statics.connection.getChat().size() != tempChatMessages.size()){
                    Statics.log("There are new messages, updating...");
                    Statics.connection.setChat(tempChatMessages);
                    return true;
                }
                else{
                    Statics.log("Chats are the same size, not updating...");
                }

            }
            else{
                Statics.log("Retrieved data is not a json array.");
            }


        }
        catch(Exception e){
            Statics.log("Exception parsing JSON: " + e.getMessage());
            e.printStackTrace();
        }

        return false;
    }


    private class leThread extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(Connection.formatUrl("/db.php?request=retrieveMessages&ticket_id=" + Statics.selectedTicket.getTicket_id()));
                Statics.log("Sending '" + url + "' to server...");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(2000);
                connection.setReadTimeout(2000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String content = "", line;
                while ((line = rd.readLine()) != null) {
                    content += line;
                }
                Statics.log("Received '" + content + "' from the server.");

                return content;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Statics.log("Error connecting to server.");
            return "error";
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String jason) {
            //update ui
            try{
                ArrayList<ChatMessage> tempChatMessages= new ArrayList<>();

                JsonElement jsonElement = new JsonParser().parse(jason);
                if(jsonElement == null){
                    Statics.log("jsonElement was null");
                    return;
                }
                if(jsonElement.isJsonArray()){
                    JsonArray jsonArray = jsonElement.getAsJsonArray();
                    for(JsonElement element : jsonArray){
                        if(element.isJsonObject()){
                            if(element == null){
                                Statics.log("Element was null");
                                continue;
                            }
                            //{"Message_id":11,"timestamp":"2019-01-31 12:33:32","Picture_location":"\/..\/foler\/pic11.png","User_id":5,"Ticket_id":3,"Message_String":"ahhhh"}
                            String messageId = element.getAsJsonObject().get("Message_id").getAsString();
                            String timestamp = element.getAsJsonObject().get("timestamp").getAsString();
                            String pictureLocation = element.getAsJsonObject().get("Picture_location").getAsString();
                            String userId = element.getAsJsonObject().get("User_id").getAsString();
                            String ticketId = element.getAsJsonObject().get("Ticket_id").getAsString();
                            String message = element.getAsJsonObject().get("Message_String").getAsString();

                            int userIdInt = Integer.parseInt(userId);
                            int messageIdInt = Integer.parseInt(messageId);
                            int ticketIdInt = Integer.parseInt(ticketId);
                            boolean isFromUs = userIdInt == Statics.userId;
                            tempChatMessages.add(new ChatMessage(message, !isFromUs, messageIdInt, timestamp, pictureLocation, userIdInt, ticketIdInt));
                        }
                        else{
                            Statics.log("Element in array is not a json object.");
                        }

                    }

                    if(Statics.connection.getChat().size() != tempChatMessages.size()){
                        Statics.log("There are new messages, updating...");
                        Statics.connection.setChat(tempChatMessages);
                        //UPDATE UI HERE
                        OpenTicketActivity.that.updateChatUI();
//                        getRecyclerView().setAdapter(new ChatListAdapter(tempChatMessages));
//                        getRecyclerView().getAdapter().notifyItemInserted(getRecyclerView().getAdapter().getItemCount() - 1);
//                        getRecyclerView().scrollToPosition(getRecyclerView().getAdapter().getItemCount() - 1);
                    }
                    else{
                        Statics.log("Chats are the same size, not updating...");
                    }

                }
                else{
                    Statics.log("Retrieved data is not a json array.");
                }


            }
            catch(Exception e){
                Statics.log("Exception parsing JSON: " + e.getMessage());
                e.printStackTrace();
            }



            if(shouldRun){
                Statics.log("Running another chat check...");
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        new leThread().execute("HMMMMM");
                    }
                }, 500);
            }
        }
    }
}
