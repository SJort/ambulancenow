package nu.nicoti.ambulancenow;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

public class PhoneActivity extends AppCompatActivity {

    private Fragment selectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        setupBottomNavigation();

        setTitle("Ticket with " + Statics.selectedHospital.getName());


        //load the initial fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.phoneFragment_container, new PhoneDetailsFragment()).commit();


        Statics.log("Created OpenTicketActivity");
    }


    public void setupBottomNavigation() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.phoneBottomNavigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_details: {
                        selectedFragment = new DetailsFragment();
                        break;
                    }
                    case R.id.nav_navigation: {
                        selectedFragment = new NavigationFragment();
                        break;
                    }
                }

                getSupportFragmentManager().beginTransaction().replace(R.id.phoneFragment_container, selectedFragment).commit();

                //select the clicked item if true
                return true;
            }
        });
    }

    //findViewById does not work for the passed view...
    public void onSendMessageButton(View view) {
        TextInputEditText messageField = findViewById(R.id.sendMessageInputField);
        if (messageField == null) {
            Statics.log("MessageField was null?");
            return;
        }
        String input = messageField.getText().toString();
        messageField.getText().clear();
        Statics.connection.sendChat(input);
        Statics.log("Sent '" + input + "'");
        if (getSelectedFragment() instanceof ChatFragment) {
            //append the new item to the chat list and scroll to it
            ChatFragment chatFragment = (ChatFragment) getSelectedFragment();
            chatFragment.getRecyclerView().getAdapter().notifyItemInserted(chatFragment.getRecyclerView().getAdapter().getItemCount() - 1);
            chatFragment.getRecyclerView().scrollToPosition(chatFragment.getRecyclerView().getAdapter().getItemCount() - 1);
        } else {
            Statics.log("Can't send chat, another fragment is open.");
        }
    }

    public Fragment getSelectedFragment() {
        return selectedFragment;
    }

}
