package nu.nicoti.ambulancenow;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.nio.charset.Charset;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;

public class Statics {

    public static Connection connection = new Connection();

    public static String notSpecified = "not specified";

    public static Hospital selectedHospital = new Hospital(notSpecified, false);
    public static Ticket selectedTicket = new Ticket(notSpecified, notSpecified, notSpecified, notSpecified, notSpecified, -1);

    public static int userId = -1;

    public static AppCompatActivity activity;

    public static boolean isLoggedIn = false;

    public static String ambulanceName = "Ambu Lancer";


    public static boolean isTicketOpen = false;
    public static String preferenceFileName = "AmbulancePreferences";




    //public static String selectedSituation = notSpecified;
    public static String[] situationStrings = {
            notSpecified,
            "Unconscious",
            "Shock",
            "Blood loss",
            "Poisoned",
            "Overdose",
            "Allergy"
    };

    //public static String selectedGender = notSpecified;
    public static String[] genderStrings = {
            notSpecified,
            "Male",
            "Female"
    };

    //public static String selectedBloodGroup = notSpecified;
    public static String[] bloodGroupStrings = {
            notSpecified,
            "A",
            "B",
            "AB",
            "O"
    };

    //public static String additionalInformation = notSpecified;

    public static boolean isErrorMessage(String message){
        if(message.toLowerCase().startsWith("error")){
            log("'" + message + "' is a error message!");
            return true;
        }
        return false;
    }

    public static boolean isSuccessMessage(String message){
        if(message.toLowerCase().startsWith("success")){
            log("'" + message + "' is a success message!");
            return true;
        }
        return false;
    }


    public static void log(String message) {
        Log.d("AmbulanceNow", message);
    }

    public static void toast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static String generateRandomString() {
        byte[] array = new byte[7];
        new Random().nextBytes(array);
        return new String(array, Charset.forName("UTF-8"));
    }


}
