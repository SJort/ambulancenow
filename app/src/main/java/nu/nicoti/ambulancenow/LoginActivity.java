package nu.nicoti.ambulancenow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginActivity extends AppCompatActivity implements ServerResponseListener {

    private String login;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        TextInputEditText loginField = findViewById(R.id.loginField);
        TextInputEditText passwordField = findViewById(R.id.passwordField);
        loginField.setText("");
        passwordField.setText("");
    }


    public void onSubmit(View view) {
        TextInputEditText loginField = findViewById(R.id.loginField);
        TextInputEditText passwordField = findViewById(R.id.passwordField);
        login = loginField.getText().toString();
        password = passwordField.getText().toString();
        Statics.connection.validateLogin(this, login, password);
    }

    @Override
    public void onServerResponse(String message) {
        if (!Statics.isErrorMessage(message)) {
            Statics.log("A C C E S S   G R A N T E D");
            Toast.makeText(LoginActivity.this, "Access granted.", Toast.LENGTH_SHORT).show();
            parseUserDetails(message);

            Statics.isLoggedIn = true;
            SharedPreferences.Editor editor = getSharedPreferences(Statics.preferenceFileName, MODE_PRIVATE).edit();
            editor.putString("login", login);
            editor.putString("password", password);
            editor.apply();
            LoginActivity.this.startActivity(new Intent(LoginActivity.this, MainActivity.class));
        } else {
            Statics.isLoggedIn = false;
            Toast.makeText(LoginActivity.this, "Credentials not recognized.", Toast.LENGTH_SHORT).show();
        }
    }

    public void parseUserDetails(String jason) {
        try {
            JsonElement jsonElement = new JsonParser().parse(jason);
            if (jsonElement.isJsonArray()) {
                JsonArray jsonArray = jsonElement.getAsJsonArray();
                for (JsonElement element : jsonArray) {
                    if (element.isJsonObject()) {
                        //{"Message_id":11,"timestamp":"2019-01-31 12:33:32","Picture_location":"\/..\/foler\/pic11.png","User_id":5,"Ticket_id":3,"Message_String":"ahhhh"}
                        String userId = element.getAsJsonObject().get("User_id").getAsString();
                        Statics.userId = Integer.parseInt(userId);
                        Statics.log("Parsed user id " + userId);
                    } else {
                        Statics.log("Element in array is not a json object.");
                    }

                }
            } else {
                Statics.log("Retrieved data is not a json array.");
            }


        } catch (Exception e) {
            Statics.log("Exception parsing JSON: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
