package nu.nicoti.ambulancenow;

public interface ServerResponseListener {
    void onServerResponse(String message);
}
