package nu.nicoti.ambulancenow;

import com.google.android.gms.maps.model.LatLng;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;

public class Hospital {

    private int hospitalId;

    private String name;
    private boolean onNetwork;
    private int imageResource;
    private String secondLine;
    private LatLng coordinates = new LatLng(51.9105467,4.4682153);
    private String phoneNumber = "+31 6 34560009";
    private String address = "Doctor Molewaterplein, 3015 CP Rotterdam";
    private String geoAddress = "geo:Doctor Molewaterplein?q=Erasmus MC";

    public Hospital(String name, boolean onNetwork) {
        this.name = name;
        this.onNetwork = onNetwork;
        if(onNetwork){
            imageResource = R.drawable.ic_check;
            secondLine = "";
        }
        else{
            imageResource = R.drawable.ic_local_phone;
            secondLine = "calls only";
        }
    }

    public Hospital(String name, boolean onNetwork, String address, String geoAddress, String phoneNumber, int hospitalId){
        this.name = name;
        this.onNetwork = onNetwork;
        this.phoneNumber = phoneNumber;
        if(onNetwork){
            imageResource = R.drawable.ic_check;
            secondLine = "";
        }
        else{
            imageResource = R.drawable.ic_local_phone;
            secondLine = "calls only";
        }

        this.address = address;
        this.geoAddress = geoAddress;
        this.hospitalId = hospitalId;
    }

    public String getName() {
        return name;
    }

    public String getSecondLine(){
        return secondLine;
    }

    public int getImageResource() {
        return imageResource;
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalId=" + hospitalId +
                ", name='" + name + '\'' +
                ", onNetwork=" + onNetwork +
                ", imageResource=" + imageResource +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", geoAddress='" + geoAddress + '\'' +
                '}';
    }

    public int getHospitalId(){
        return hospitalId;
    }

    public String getAddress() {
        return address;
    }

    public String getMapsGeoAddress() {
        return geoAddress;
    }


    public LatLng getCoordinates(){
        return coordinates;
    }

    public boolean isOnNetwork(){
        return onNetwork;
    }

    public double getDistance(){
        //formula that calculates the distance to the hospital based on the mass of the sun on the first high tide of tuesday
        return new BigDecimal((Math.random() * 100) / 3.0).setScale(1, RoundingMode.CEILING).doubleValue();
    }

    public String getPhoneNumber(){
        return phoneNumber;
    }

}
