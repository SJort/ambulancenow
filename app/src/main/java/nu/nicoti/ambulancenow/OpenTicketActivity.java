package nu.nicoti.ambulancenow;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class OpenTicketActivity extends AppCompatActivity {

    private Fragment selectedFragment;
    private GoogleMap mMap;
    private MapFragment mMapFragment;

    public static OpenTicketActivity that;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        that = this;
        Statics.log("OpenTicketActivity onCreate()");
        setContentView(R.layout.activity_open_ticket); //change to map fragment?
        Statics.activity = this;

        setupBottomNavigation();

        setTitle("Ticket with " + Statics.selectedHospital.getName());


        //load the initial fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DetailsFragment()).commit();


        Statics.log("Created OpenTicketActivity");
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(OpenTicketActivity.this, "Finish the ticket first", Toast.LENGTH_SHORT).show();
    }

    public void setupBottomNavigation() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_details: {
                        selectedFragment = new DetailsFragment();
                        break;
                    }
                    case R.id.nav_chat: {
                        selectedFragment = new ChatFragment();
                        break;
                    }
                    case R.id.nav_navigation: {
                        selectedFragment = new NavigationFragment();
                        //selectedFragment = new nu.nicoti.ambulancenow.MapFragment();
                        break;
                    }
                }

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();


                //select the clicked item if true
                return true;
            }
        });
    }

    //findViewById does not work for the passed view...
    public void onSendMessageButton(View view) {
        TextInputEditText messageField = findViewById(R.id.sendMessageInputField);
        if (messageField == null) {
            Statics.log("MessageField was null?");
            return;
        }
        String input = messageField.getText().toString();
        messageField.getText().clear();
        Statics.connection.sendChat(input);
        Statics.log("Sent '" + input + "'");
        //updateChatUI();
    }

    public Fragment getSelectedFragment() {
        return selectedFragment;
    }

    public void updateChatUI(){
        Statics.log("OnChatMessage in OpenTicketActivity");

        if (getSelectedFragment() instanceof ChatFragment) {
            //append the new item to the chat list and scroll to it
            ChatFragment chatFragment = (ChatFragment) getSelectedFragment();
            chatFragment.getRecyclerView().setAdapter(new ChatListAdapter(Statics.connection.getChat()));
            chatFragment.getRecyclerView().getAdapter().notifyItemInserted(chatFragment.getRecyclerView().getAdapter().getItemCount() - 1);
            chatFragment.getRecyclerView().scrollToPosition(chatFragment.getRecyclerView().getAdapter().getItemCount() - 1);
        } else {
            Statics.log("Can't send chat, another fragment is open.");
        }
    }

}
