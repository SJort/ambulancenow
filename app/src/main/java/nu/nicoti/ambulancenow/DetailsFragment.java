package nu.nicoti.ambulancenow;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupTextFields(view);

        setupFinishButton();
        setupCallButton();
    }

    public void setupFinishButton(){
        Button button = getActivity().findViewById(R.id.finishButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Statics.isTicketOpen = false;
                Toast.makeText(getActivity(), "Finished ticket with " + Statics.selectedHospital.getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    public void setupCallButton(){
        Button button = getActivity().findViewById(R.id.callHospitalButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = Statics.selectedHospital.getPhoneNumber();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        });
    }

    public void setupTextFields(View view){
//        TextView nameText = view.findViewById(R.id.nameView);
//        nameText.setText("Name: " + Statics.selectedTicket.getName());

        TextView bloodGroupText = view.findViewById(R.id.bloodGroupText);
        bloodGroupText.setText("Blood group: " + Statics.selectedTicket.getBloodType());

        TextView situationText = view.findViewById(R.id.situationText);
        situationText.setText("Situation: " + Statics.selectedTicket.getSituation());

        TextView genderText = view.findViewById(R.id.genderText);
        genderText.setText("Gender: " + Statics.selectedTicket.getGender());

        TextView additionalInformationText = view.findViewById(R.id.additionalDetailsText);
        additionalInformationText.setText("Additional information: " + Statics.selectedTicket.getOtherInformation());

        TextView phoneNumberText = view.findViewById(R.id.hospitalPhone);
        phoneNumberText.setText("Phone: " + Statics.selectedHospital.getPhoneNumber());
    }

}
