package nu.nicoti.ambulancenow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Statics.log("MainActivity onCreate()");
        setContentView(R.layout.activity_main);
        openNeededActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        openNeededActivity();
    }

    public void openNeededActivity(){
        boolean testing = false;
        if(testing){
            //MainActivity.this.startActivity(new Intent(MainActivity.this, MapsActivity.class));
            return;
        }

        if(isLoggedIn()){
            Statics.log("We are already logged in.");
            Statics.log("Checking if there is already a ticket open...");
            if(Statics.isTicketOpen){
                Statics.log("There is a ticket open.");
                Intent myIntent = new Intent(MainActivity.this, OpenTicketActivity.class);
                MainActivity.this.startActivity(myIntent);
                Toast.makeText(MainActivity.this, "Resumed ticket with " + Statics.selectedHospital.getName(), Toast.LENGTH_SHORT).show();
            }
            else{
                MainActivity.this.startActivity(new Intent(MainActivity.this, HospitalListActivity.class));
            }
        }
        else{
            Statics.log("We are not logged in.");
            MainActivity.this.startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }


    }

    public String getLogin(){
        SharedPreferences prefs = getSharedPreferences(Statics.preferenceFileName, MODE_PRIVATE);
        String login = prefs.getString("login", null);
        return login;
    }

    public String getPassword(){
        SharedPreferences prefs = getSharedPreferences(Statics.preferenceFileName, MODE_PRIVATE);
        String password = prefs.getString("password", null);
        return password;
    }

    public boolean isLoggedIn(){
        return Statics.isLoggedIn;
        //return true;
        //return false;
        //return getLogin() != null && getPassword() != null;
    }
}
