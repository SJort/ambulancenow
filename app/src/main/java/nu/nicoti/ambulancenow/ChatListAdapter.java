package nu.nicoti.ambulancenow;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> implements Filterable {

    private ArrayList<ChatMessage> mChatList;
    private ArrayList<ChatMessage> mFullChatList;
    private OnItemClickListener mListener;

    public ChatListAdapter(ArrayList<ChatMessage> exampleList) {
        mChatList = exampleList;
        //makes a copy
        mFullChatList = new ArrayList<>(mChatList);
    }

    public interface OnItemClickListener{
        void onItemClicked(int position);
    }


    public void setOnItemClickedListener(OnItemClickListener listener){
        mListener = listener;
    }

    //it needs a viewholder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView receivedMessage;
        public TextView sendMessage;

        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            receivedMessage = itemView.findViewById(R.id.chatReceivedMessage);
            sendMessage = itemView.findViewById(R.id.chatSentMessage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClicked(position);
                        }
                    }
                }
            });
        }
    }

    //we have to pass the layout of the cards here, to the adapter
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item, parent, false);
        ViewHolder evh = new ViewHolder(v, mListener);
        return evh;
    }

    //method to pass values to the references of the textviews etc
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ChatMessage message = mChatList.get(position);
        if(message.isReceived()){
            viewHolder.receivedMessage.setText(message.getMessage());
            viewHolder.sendMessage.setText("");
        }
        else{
            viewHolder.sendMessage.setText(message.getMessage());
            viewHolder.receivedMessage.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return mChatList.size();
    }

    @Override
    public Filter getFilter() {
        return chatFilter;
    }

    private Filter chatFilter = new Filter() {
        //this method runs in a separate thread to avoid lag
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ChatMessage> filteredList = new ArrayList<>();
            if(constraint == null || constraint.length() == 0){
                //empty, so show everything
                filteredList.addAll(mFullChatList);
            }
            else{
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(ChatMessage item : mFullChatList){
                    if(item != null){
                        if(item.getMessage().toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        //retrieved shit from performFiltering
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mChatList.clear();
            mChatList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}
