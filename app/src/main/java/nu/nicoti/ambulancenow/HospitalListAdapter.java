package nu.nicoti.ambulancenow;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class HospitalListAdapter extends RecyclerView.Adapter<HospitalListAdapter.ViewHolder> implements Filterable {

    private ArrayList<Hospital> mHospitalList;
    private ArrayList<Hospital> mFullHospitalList;
    private OnItemClickListener mListener;

    public HospitalListAdapter(ArrayList<Hospital> exampleList) {
        mHospitalList = exampleList;
        //makes a copy
        mFullHospitalList = new ArrayList<>(mHospitalList);
    }

    public interface OnItemClickListener{
        void onItemClicked(int position);
    }


    public void setOnItemClickedListener(OnItemClickListener listener){
        mListener = listener;
    }

    //it needs a viewholder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public TextView mHospitalName;
        public TextView mTextView2;
        public TextView hospitalDistance;

        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.hospitalmageView);
            mHospitalName = itemView.findViewById(R.id.hospitalTextView1);
            mTextView2 = itemView.findViewById(R.id.hospitalTextView2);
            hospitalDistance = itemView.findViewById(R.id.hospitalDistance);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClicked(position);
                        }
                    }
                }
            });
        }
    }

    //we have to pass the layout of the cards here, to the adapter
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.hospital_list_item, parent, false);
        ViewHolder evh = new ViewHolder(v, mListener);
        return evh;
    }

    //method to pass values to the references of the textviews etc
    @Override
    public void onBindViewHolder(@NonNull ViewHolder exampleViewHolder, int position) {
        Hospital currentItem = mHospitalList.get(position);
        exampleViewHolder.mImageView.setImageResource(currentItem.getImageResource());
        exampleViewHolder.mHospitalName.setText(currentItem.getName());
        exampleViewHolder.mTextView2.setText(currentItem.getSecondLine());
        exampleViewHolder.hospitalDistance.setText(currentItem.getDistance() + "km");
    }

    @Override
    public int getItemCount() {
        return mHospitalList.size();
    }

    @Override
    public Filter getFilter() {
        return hospitalFilter;
    }

    private Filter hospitalFilter = new Filter() {
        //this method runs in a separate thread to avoid lag
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Hospital> filteredList = new ArrayList<>();
            if(constraint == null || constraint.length() == 0){
                //empty, so show everything
                filteredList.addAll(mFullHospitalList);
            }
            else{
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(Hospital item : mFullHospitalList){
                    if(item != null){
                        if(item.getName().toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        //retrieved shit from performFiltering
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mHospitalList.clear();
            mHospitalList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}
