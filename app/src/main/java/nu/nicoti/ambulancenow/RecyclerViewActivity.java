package nu.nicoti.ambulancenow;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

public class RecyclerViewActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    //basically the bridge between data and the view, makes the view not load the whole arraylist but only what it needs
    private ChatListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        final ArrayList<ChatMessage> chatMessageList = Statics.connection.getChat();

        mRecyclerView = findViewById(R.id.hospitalRecyclerView);
        mRecyclerView.setHasFixedSize(true); //increases performance, when you know the view wont change in size
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ChatListAdapter(chatMessageList);


        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickedListener(new ChatListAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("HospitalChatMessage", chatMessageList.get(position).getMessage()); //do something with it
                clipboard.setPrimaryClip(clip);

                Toast.makeText(RecyclerViewActivity.this, "Copied '" + chatMessageList.get(position).getMessage() + "' to clipboard", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //this activates our custom menu with search
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.hospital_search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)searchItem.getActionView();

        //now its basically a text field or something
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}
