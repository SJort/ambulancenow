package nu.nicoti.ambulancenow;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;

public class HospitalListActivity extends AppCompatActivity implements ServerResponseListener{

    private RecyclerView mRecyclerView;
    //basically the bridge between data and the view, makes the view not load the whole arraylist but only what it needs
    private HospitalListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_list);

        setTitle("Select hospital to contact");

        Statics.connection.requestHospitals(this);


    }

    public void setupList(){
        final ArrayList<Hospital> hospitalList = Statics.connection.getHospitals();

        mRecyclerView = findViewById(R.id.hospitalRecyclerView);
        mRecyclerView.setHasFixedSize(true); //increases performance, when you know the view wont change in size
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new HospitalListAdapter(hospitalList);


        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickedListener(new HospitalListAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                String clickedHospital = hospitalList.get(position).getName();
                Statics.log("Selected " + clickedHospital);
                Toast.makeText(HospitalListActivity.this, "Selected " + clickedHospital, Toast.LENGTH_SHORT).show();
                determineHospital(clickedHospital);


                if(Statics.selectedHospital.isOnNetwork()){
                    HospitalListActivity.this.startActivity(new Intent(HospitalListActivity.this, TicketActivity.class));
                }
                else{
                    HospitalListActivity.this.startActivity(new Intent(HospitalListActivity.this, PhoneActivity.class));
                }

            }
        });
    }

    //this activates our custom menu with search
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.hospital_search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)searchItem.getActionView();

        //now its basically a text field or something
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }


    public void determineHospital(String input) {
        for (Hospital hospital : Statics.connection.getHospitals()) {
            if (hospital != null) {
                if (hospital.getName().equalsIgnoreCase(input)) {
                    Statics.selectedHospital = hospital;
                    return;
                }
            }
        }
        Statics.log("Couldn't find selected hospital.");
        Statics.selectedHospital = null;
    }

    @Override
    public void onServerResponse(String message) {
        Statics.log("OnServerResponse in HospitalListActivity");
        Statics.log("Received: '" + message + "'.");
        parse(message);
        setupList();
    }

    public void parse(String jason) {
        try{
            JsonElement jsonElement = new JsonParser().parse(jason);
            if(jsonElement.isJsonArray()){
                JsonArray jsonArray = jsonElement.getAsJsonArray();
                for(JsonElement element : jsonArray){
                    if(element.isJsonObject()){
                        //[{"name":"erasmusmc","address":"naast dijkzigt","geoAddress":"0850982048302graden zuid","Hospital_id":1,"phoneNumber":"+31 06 0123456","usesNicotin":1}]
                        String name = element.getAsJsonObject().get("name").getAsString();
                        String address = element.getAsJsonObject().get("address").getAsString();
                        String geoAddress = element.getAsJsonObject().get("geoAddress").getAsString();
                        String phoneNumber = element.getAsJsonObject().get("phoneNumber").getAsString();
                        String onNetwork = element.getAsJsonObject().get("usesNicotin").getAsString();
                        String id = element.getAsJsonObject().get("Hospital_id").getAsString();

                        onNetwork = onNetwork.equals("1") ? "true" : "false";
                        boolean isOnNetwork = Boolean.parseBoolean(onNetwork);
                        Hospital toAdd = new Hospital(name, isOnNetwork, address, geoAddress, phoneNumber, Integer.parseInt(id));
                        Statics.log("Adding hospital " + toAdd.toString() + " to hospitals...");
                        Statics.connection.getHospitals().add(toAdd);
                    }
                    else{
                        Statics.log("Element in array is not a json object.");
                    }

                }
            }
            else{
                Statics.log("Retrieved data is not a json array.");
            }


        }
        catch(Exception e){
            Statics.log("Exception parsing JSON: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
