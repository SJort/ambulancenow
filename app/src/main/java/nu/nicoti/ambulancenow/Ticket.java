package nu.nicoti.ambulancenow;

public class Ticket {
    //http://62.250.89.183/db.php?request=sendTicket&name=slkdf&bloodtype=A&gender=23&situation=lol&misc=prank&hospital_id=1

    private String bloodType;
    private String gender;
    private String situation;
    private String otherInformation;
    private int hospitalId;
    private int ticket_id = 1;
    private String name;

    public Ticket(String name, String bloodType, String gender, String situation, String otherInformation, int hospitalId) {
        this.name = name;
        this.bloodType = bloodType;
        this.gender = gender;
        this.situation = situation;
        this.otherInformation = otherInformation;
        this.hospitalId = hospitalId;
    }

    public String getName(){
        return name;
    }

    public String getBloodType() {
        return bloodType;
    }

    public String getGender() {
        return gender;
    }

    public String getSituation() {
        return situation;
    }

    public String getOtherInformation() {
        return otherInformation;
    }

    public int getHospitalId() {
        return hospitalId;
    }

    public int getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(int ticket_id) {
        this.ticket_id = ticket_id;
    }
}
