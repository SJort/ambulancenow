package nu.nicoti.ambulancenow;

public interface ChatListener {
    void onChatMessage(ChatMessage message);
}
