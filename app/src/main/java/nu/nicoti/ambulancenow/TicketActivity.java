package nu.nicoti.ambulancenow;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Arrays;

public class TicketActivity extends AppCompatActivity implements ServerResponseListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle("Making ticket for " + Statics.selectedHospital.getName());

        setupDoneButton();
        setupSpinners();

        Statics.log("Loaded TicketActivity");
    }

    public void setupSpinners(){
        setupSpinnerWithArray((Spinner)findViewById(R.id.situationSpinner), Statics.situationStrings);
        setupSpinnerWithArray((Spinner)findViewById(R.id.bloodGroupSpinner), Statics.bloodGroupStrings);
        setupSpinnerWithArray((Spinner)findViewById(R.id.genderSpinner), Statics.genderStrings);
    }


    public void setupSpinnerWithArray(Spinner spinner, String[] array){
        ArrayAdapter adapter = new ArrayAdapter(TicketActivity.this, android.R.layout.simple_spinner_item, Arrays.asList(array));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }


    public void setupDoneButton(){
        Button doneButton= findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDoneButton();
            }
        });
    }

    public void onDoneButton() {
        Toast.makeText(TicketActivity.this, "Sending ticket to " + Statics.selectedHospital.getName() + "...", Toast.LENGTH_SHORT).show();


        //retrieve values from fields

        String name = ((EditText)findViewById(R.id.nameInput)).getText().toString();
        if(name.isEmpty()){
            name = Statics.notSpecified;
        }
        String selectedGender = ((Spinner)findViewById(R.id.genderSpinner)).getSelectedItem().toString();
        String selectedSituation = ((Spinner)findViewById(R.id.situationSpinner)).getSelectedItem().toString();
        String selectedBloodGroup = ((Spinner)findViewById(R.id.bloodGroupSpinner)).getSelectedItem().toString();
        String additionalInformation = ((EditText)findViewById(R.id.additionalDetailsInputField)).getText().toString();
        if(additionalInformation.isEmpty()){
            additionalInformation = Statics.notSpecified;
        }
        Statics.selectedTicket = new Ticket(name, selectedBloodGroup, selectedGender, selectedSituation, additionalInformation, Statics.selectedHospital.getHospitalId());
        Statics.connection.sendTicket(this, Statics.selectedTicket);

    }

    @Override
    public void onServerResponse(String message) {
        if(Statics.isSuccessMessage(message)){
            if(message != null){
                String[] response = message.split("=");
                if(response.length > 1){
                    String idea = response[1];
                    int id = Integer.parseInt(idea);
                    Statics.selectedTicket.setTicket_id(id);

                    Intent myIntent = new Intent(TicketActivity.this, OpenTicketActivity.class);
                    TicketActivity.this.startActivity(myIntent);
                    Statics.isTicketOpen = true;
                    Toast.makeText(TicketActivity.this, "Sent ticket to " + Statics.selectedHospital.getName() + "!", Toast.LENGTH_SHORT).show();
                    Statics.log("Successfully parsed sent ticket and parsed ticket_id " + id);
                }
            }
        }
        else{
            Toast.makeText(TicketActivity.this, "Error sending ticket to " + Statics.selectedHospital.getName() + "!", Toast.LENGTH_SHORT).show();
        }
    }



    //on back button in title bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }



}
