package nu.nicoti.ambulancenow;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.Thread.sleep;

public class Connection implements ServerResponseListener{

    private static String ip = "http://62.250.89.183";
    private ArrayList<Hospital> hospitals = new ArrayList<>();
    private ArrayList<ChatMessage> chat;
    private ServerResponseListener serverResponseListener;

    public Connection() {
        setup();
    }

    public void sendTicket(ServerResponseListener serverResponseListener, Ticket ticket) {
        //http://62.250.89.183/db.php?request=sendTicket&name=slkdf&bloodtype=A&gender=23&situation=lol&misc=prank&hospital_id=1
        String url = formatUrl("/db.php?request=sendTicket&name=" + ticket.getName()
                + "&bloodtype=" + ticket.getBloodType()
                + "&gender=" + ticket.getGender()
                + "&situation=" + ticket.getSituation()
                + "&misc=" + ticket.getOtherInformation()
                + "&hospital_id=" + ticket.getHospitalId());

        this.serverResponseListener = serverResponseListener;
        new GetUrlContentTask().execute(url);
    }



    public ArrayList<Hospital> getHospitals() {
        return hospitals;
    }

    public void requestHospitals(ServerResponseListener serverResponseListener) {
        String url = formatUrl("/db.php?request=getHospitals");
        this.serverResponseListener = serverResponseListener;
        new GetUrlContentTask().execute(url);
    }

    public void validateLogin(ServerResponseListener serverResponseListener, String login, String password) {
        String url = formatUrl("/db.php?request=login&password=" + password + "&username=" + login);
        this.serverResponseListener = serverResponseListener;
        new GetUrlContentTask().execute(url);
    }

    public ArrayList<ChatMessage> getChat() {
        return chat;
    }

    public void sendChat(String message) {
        String url = formatUrl("/db.php?request=sendMessage&ticket_id=" + Statics.selectedTicket.getTicket_id() + "&user_id=" + Statics.userId + "&message=" + message);
        this.serverResponseListener = this;
        new GetUrlContentTask().execute(url);

        //NOT DOING THIS, it will be done through the server
        //chat.add(new ChatMessage(message, false));
    }

    public void setChat(ArrayList<ChatMessage> messages){
        this.chat = messages;
    }

    @Override
    public void onServerResponse(String message) {
        if(message != null && Statics.isSuccessMessage(message)){
            Statics.log("Successfully send chat message!");
        }
        else{
            Statics.log("Error sending chat message.");
        }
    }


    public void setup() {
        chat = new ArrayList<>(Arrays.asList(new ChatMessage("hey hospital", false),
                new ChatMessage("sup, ambulance", true)));
    }

    public static String formatUrl(String url){
        return ip + url.replace(" ", "%20");
    }


    public class GetUrlContentTask extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                Statics.log("Sending '" + url + "' to server...");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(2000);
                connection.setReadTimeout(2000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String content = "", line;
                while ((line = rd.readLine()) != null) {
                    content += line;
                }
                Statics.log("Received '" + content + "' from the server.");

                return content;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Statics.log("Error connecting to server.");
            return "error";
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            Statics.log("Sending server response to listener...");
            if (serverResponseListener == null) {
                Statics.log("ServerResponseListener was null.");
                return;
            }
            if (result == null) {
                Statics.log("Response was null.");
                return;
            }
            serverResponseListener.onServerResponse(result);
        }
    }

}
